import { Branch } from 'src/branch/entities/branch.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class StoreExpenses {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  electricBill: number;

  @Column()
  waterBill: number;

  @Column()
  total: number;

  @Column()
  due_date: string;

  @Column()
  status: string;

  @ManyToOne(() => Branch, (branch) => branch.storeExpenses)
  branch: Branch;
}
