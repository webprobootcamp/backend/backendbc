import { Test, TestingModule } from '@nestjs/testing';
import { StoreexpensesController } from './storeexpenses.controller';
import { StoreexpensesService } from './storeexpenses.service';

describe('StoreexpensesController', () => {
  let controller: StoreexpensesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StoreexpensesController],
      providers: [StoreexpensesService],
    }).compile();

    controller = module.get<StoreexpensesController>(StoreexpensesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
