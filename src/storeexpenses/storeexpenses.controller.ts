import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { StoreexpensesService } from './storeexpenses.service';
import { CreateStoreexpenseDto } from './dto/create-storeexpense.dto';
import { UpdateStoreexpenseDto } from './dto/update-storeexpense.dto';

@Controller('storeexpenses')
export class StoreexpensesController {
  constructor(private readonly storeexpensesService: StoreexpensesService) {}

  @Post()
  create(@Body() createStoreexpenseDto: CreateStoreexpenseDto) {
    return this.storeexpensesService.create(createStoreexpenseDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.storeexpensesService.findOne(+id);
  }

  @Get()
  findAll() {
    return this.storeexpensesService.findAll();
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateStoreexpenseDto: UpdateStoreexpenseDto,
  ) {
    return this.storeexpensesService.update(+id, updateStoreexpenseDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.storeexpensesService.remove(+id);
  }

  @Get('reportexpenses1')
  reportexpenses1() {
    return this.storeexpensesService.reportexpenses1();
  }
}
