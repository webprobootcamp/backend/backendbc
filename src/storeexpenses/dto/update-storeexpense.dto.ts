import { PartialType } from '@nestjs/swagger';
import { CreateStoreexpenseDto } from './create-storeexpense.dto';

export class UpdateStoreexpenseDto extends PartialType(CreateStoreexpenseDto) {}
