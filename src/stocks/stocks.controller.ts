/* eslint-disable prettier/prettier */
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { StocksService } from './stocks.service';
import { CreateReportDto, CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { uuid } from 'uuidv4';
import { extname } from 'path';

@Controller('stocks')
export class StocksController {
  constructor(private readonly StockService: StocksService) {}
  @Post('storeStock')
  findAllspp(@Body() createEmployeeDto: CreateReportDto) {
    return this.StockService.findAllspp(
      createEmployeeDto.status,
      createEmployeeDto.unit + '',
    );
  }
  // Create
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/stocks',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createStockDto: CreateStockDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      createStockDto.image = file.filename;
    }
    return this.StockService.create(createStockDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.StockService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.StockService.findOne(+id);
  }
  // Partial Update
  @Post(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/stocks',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updateStockDto: UpdateStockDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updateStockDto.image = file.filename;
    }
    return this.StockService.update(+id, updateStockDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.StockService.remove(+id);
  }
}
