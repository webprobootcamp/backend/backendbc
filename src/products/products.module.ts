import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { ReceiptItem } from 'src/receipts/entities/receiptItem.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Product, ReceiptItem])],
  controllers: [ProductsController],
  providers: [ProductsService],
})
export class ProductsModule {}
