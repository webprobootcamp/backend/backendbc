import { OrderItem } from 'src/orders/entities/orderItem.entity';
import { ReceiptItem } from 'src/receipts/entities/receiptItem.entity';
import { Type } from 'src/types/entities/type.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @Column({ nullable: true })
  typeId: number;

  @ManyToOne(() => Type, (type) => type.products)
  type: Type;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
  orderItems: OrderItem[];

  @OneToMany(() => ReceiptItem, (receiptItem) => receiptItem.product, {
    onDelete: 'CASCADE',
  })
  receiptItems: ReceiptItem[];
}
