import { Test, TestingModule } from '@nestjs/testing';
import { UsestockOrdersService } from './usestock-orders.service';

describe('UsestockOrdersService', () => {
  let service: UsestockOrdersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsestockOrdersService],
    }).compile();

    service = module.get<UsestockOrdersService>(UsestockOrdersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
