export class CreateUserDto {
  email: string;
  password: string;
  name: string;
  gender: string;
  roles: string;
  emp: string;
  image: string;
  q;
}
