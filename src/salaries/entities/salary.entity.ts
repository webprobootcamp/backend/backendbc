/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  pay_date: string;

  @Column()
  emp_id: number;
  // empId: employee.id;

  @Column()
  emp_name: string;
  // name: employee.name;

  @Column()
  type: string;

  @Column()
  pay_period: string;

  @Column()
  pay: number;

  @Column()
  work_hour: number;

  // @ManyToOne(() => Employee, (employee) => employee.salaries)
  // @JoinColumn()
  // employee: Employee;
}
