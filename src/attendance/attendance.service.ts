/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Attendance } from './entities/attendance.entity';
import { CreateAttendanceDto } from './dto/create-attendance.dto';
import { Employee } from 'src/employees/entities/employee.entity';
import { workerData } from 'worker_threads';
import { CreateTypeDto } from 'src/types/dto/create-type.dto';
import { UpdateAttendanceDto } from './dto/update-attendance.dto';

@Injectable()
export class AttendanceService {
  constructor(
    @InjectRepository(Attendance)
    private readonly attendanceRepository: Repository<Attendance>,
    @InjectRepository(Employee)
    private readonly employeeRepository: Repository<Employee>,
  ) {}

  findAll() {
    try {
      return this.attendanceRepository.find({ relations: ['employee'] });
    } catch (error) {
      console.log(error);
    }
  }
  findOne(id: number) {
    return this.attendanceRepository.findOne({
      where: { id },
      relations: { employee: true },
    });
  }

  remove(id: number) {
    return `This action removes a #${id} attendance`;
  }

  async createAttendance(createAttendanceDto: CreateAttendanceDto) {
    try {
      const employee = await this.employeeRepository.findOne({
        where: { id: +createAttendanceDto.employeeId },
      });
      if (!employee) {
        throw new Error('Employee not found');
      }
      console.log(employee.name);
      const attendance = new Attendance();
      attendance.employeeId = employee.id;
      attendance.name = employee.name;
      attendance.date = new Date();
      console.log(attendance.date);

      attendance.clockIn = new Date().toLocaleTimeString();
      if (attendance.clockIn > '09:00:00') {
        attendance.status = 'Late';
      } else {
        attendance.status = 'On Time';
      }
      attendance.clockOut = '00:00:00';
      attendance.workedTime = 0;

      // const today = new Date().toLocaleDateString();
      // if (new Date(attendance.date).toLocaleDateString() === today) {
      //   throw new Error('You can only clock in once a day');
      // }

      return this.attendanceRepository.save(attendance);
    } catch (error) {
      console.log(error);
    }
  }

  async updateAttendance(id: string) {
    try {
      const attendance = await this.attendanceRepository.findOne({
        where: { employeeId: +id, clockOut: null },
        order: { clockIn: 'DESC' },
      });
      console.log(attendance);

      if (!attendance) {
        throw new Error('Attendance not found');
      }

      // const today = new Date().toLocaleDateString();
      // if (new Date(attendance.date).toLocaleDateString() !== today) {
      //   throw new Error('You can only clock out once a day');
      // }

      attendance.clockOut = new Date().toLocaleTimeString();
      const startTime = new Date('2024-04-02T' + attendance.clockIn + '.150Z');
      const endTime = new Date('2024-04-02T' + attendance.clockOut + '.150Z');

      if (endTime < startTime) {
        endTime.setDate(endTime.getDate() + 1);
      }

      const workedTimeInMilliseconds = endTime.getTime() - startTime.getTime();
      const workedTimeInHours = workedTimeInMilliseconds / (1000 * 60 * 60);
      attendance.workedTime = workedTimeInHours;

      console.log('Clock Out Time:', attendance.clockOut);
      console.log('Worked Time (Hours):', workedTimeInHours);

      return await this.attendanceRepository.save(attendance);
    } catch (error) {
      console.log(error);
      throw new Error('Failed to update attendance');
    }
  }

  async findLastAttendanceByEmployeeId(employeeId: number) {
    try {
      const employee = await this.employeeRepository.findOne({
        where: { id: employeeId },
      });

      if (!employee) {
        throw new Error('Employee not found');
      }

      return await this.attendanceRepository.findOne({
        where: { employeeId: employeeId },
        order: { date: 'DESC' },
      });
    } catch (error) {
      console.log(error);
    }
  }
}
