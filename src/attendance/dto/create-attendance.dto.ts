/* eslint-disable prettier/prettier */
// create-attendance.dto.ts
import { IsString } from 'class-validator';

export class CreateAttendanceDto {
  @IsString()
  employeeId: string;

  @IsString()
  name: string;

  @IsString()
  date: string;

  @IsString()
  clockIn?: string;

  @IsString()
  clockOut: string;

  @IsString()
  status: string;

  @IsString()
  workedTime: string;
}
