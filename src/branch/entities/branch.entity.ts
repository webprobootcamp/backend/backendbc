import { Employee } from 'src/employees/entities/employee.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Stock } from 'src/stocks/entities/stock.entity';
import { StoreExpenses } from 'src/storeexpenses/entities/storeexpense.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  maneger: string;

  @Column()
  address: string;

  @Column()
  tels: string;

  @OneToMany(() => Stock, (stock) => stock.branch)
  stock: Stock[];

  @OneToMany(() => Receipt, (receipt) => receipt.branch, {
    onDelete: 'CASCADE',
  })
  receipts: Receipt[];

  @OneToMany(() => Employee, (employee) => employee.branch, {
    onDelete: 'CASCADE',
  })
  employees: Employee[];

  @OneToMany(() => StoreExpenses, (storeExpenses) => storeExpenses.branch)
  storeExpenses: StoreExpenses[];
}
