import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common';
import { BuystockOrdersService } from './buystock-orders.service';
import { CreateBuystockOrderDto } from './dto/create-buystock-order.dto';
import { UpdateBuystockOrderDto } from './dto/update-buystock-order.dto';

@Controller('buystock-orders')
export class BuystockOrdersController {
  constructor(private readonly buystockOrdersService: BuystockOrdersService) {}

  @Post()
  create(@Body() createBuystockOrderDto: CreateBuystockOrderDto) {
    return this.buystockOrdersService.create(createBuystockOrderDto);
  }

  @Get('getBuyStockGraph1')
  findViewOrderBuyStockAvg() {
    return this.buystockOrdersService.findViewOrderBuyStockAvg();
  }

  @Get('getBuyStockGraph2')
  findViewOrderBuyStockDD() {
    return this.buystockOrdersService.findViewOrderBuyStockDD();
  }

  @Get('getBuyStockGraph3')
  findViewOrderBuyStockYY() {
    return this.buystockOrdersService.findViewOrderBuyStockYY();
  }

  @Get()
  findAll() {
    return this.buystockOrdersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.buystockOrdersService.findOne(+id);
  }

  @Post(':id')
  update(
    @Param('id') id: string,
    @Body() updateBuystockOrderDto: UpdateBuystockOrderDto,
  ) {
    return this.buystockOrdersService.update(+id, updateBuystockOrderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.buystockOrdersService.remove(+id);
  }
}
