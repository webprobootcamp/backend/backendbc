import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Member {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  tel: string;

  @Column()
  point: string;

  @Column({ nullable: true })
  spent: number;

  @OneToMany(() => Receipt, (receipt) => receipt.member)
  receipts: Receipt[];
}
