import { Product } from 'src/products/entities/product.entity';

import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Receipt } from './receipt.entity';

@Entity()
export class ReceiptItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  name: string;

  @Column()
  price: number;

  @Column()
  unit: number;

  @ManyToOne(() => Product, (product) => product.receiptItems, {
    onDelete: 'CASCADE',
  })
  product: Product;

  @ManyToOne(() => Receipt, (receipt) => receipt.receiptItems, {
    onDelete: 'CASCADE',
  })
  receipt: Receipt;
}
