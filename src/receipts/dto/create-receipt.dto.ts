import { User } from 'src/users/entities/user.entity';
import { ReceiptItem } from '../entities/receiptItem.entity';
import { Member } from 'src/members/entities/member.entity';

export class CreateReceiptDto {
  id: number;

  totalBefore: number;

  memberDiscount: number;

  promotionDiscount: number;

  totalDiscount: number;

  total: number;

  receivedAmount: number;

  cashBack: number;

  paymentType: string;

  user: User;

  receiptItems: ReceiptItem[];

  member: Member;
}
