/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { Employee } from './entities/employee.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateEmployeeDto } from './dto/update-employee.dto';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}

  create(createEmployeeDto: CreateEmployeeDto) {
    const employee = new Employee();
    employee.name = createEmployeeDto.name;
    employee.tel = createEmployeeDto.tel;
    employee.salary = parseInt(createEmployeeDto.salary);
    employee.dateIn = createEmployeeDto.dateIn;
    employee.status = JSON.parse(createEmployeeDto.status);
    if (createEmployeeDto.image && createEmployeeDto.image !== '') {
      employee.image = createEmployeeDto.image;
    }
    return this.employeeRepository.save(employee);
  }

  findAll() {
    return this.employeeRepository.find({
      relations: { status: true, branch: true },
    });
  }

  // async findAllspp(
  //   name: string,
  //   tel: string,
  //   salary: string,
  //   dateIn: string,
  //   result: number,
  //   status: string,
  //   branch: string,
  // ) {
  //   const queryString = `CALL insertEmployee(?, ?, ?, ?, ?, ?, ?, @pResult, @pMessage);SELECT @pResult AS pResult, @pMessage AS pMessage;`;
  //   console.log(queryString);

  //   const results = await this.employeeRepository.query(queryString, [
  //     name,
  //     tel,
  //     salary,
  //     dateIn,
  //     result,
  //     status,
  //     branch,
  //   ]);
  //   return results[0];

  // }

  async findAllspp(
    name: string,
    tel: string,
    salary: string,
    dateIn: string,
    status: string,
    branch: string,
  ) {
    const callQueryString = `
      CALL insertEmployee('${name}', '${tel}', '${salary}', '${dateIn}', '${status}', '${branch}', @pResult, @pMessage);
    `;
    const selectQueryString = `
      SELECT @pResult AS pResult, @pMessage AS pMessage;
    `;

    console.log(callQueryString);
    console.log(selectQueryString);

    // Execute CALL statement
    await this.employeeRepository.query(callQueryString);

    // Execute SELECT statement
    const results = await this.employeeRepository.query(selectQueryString);

    return results[0];
  }

  ///sp 2
  async findAllsppp(
    name: string,
    tel: string,
    salary: string,
    dateIn: string,
    status: string,
    branch: string,
  ) {
    try {
      const callQueryStringEmp = `
      CALL LoopEmployCounter('${name}', '${tel}', '${salary}', '${dateIn}', '${status}', '${branch}', @totalEmployees);
      `;
      const selectQueryStringEmp = `
      SELECT @totalEmployees AS totalEmployees;
    `;

      console.log(callQueryStringEmp);
      console.log(selectQueryStringEmp);

      // Execute SELECT statement
      const results = await this.employeeRepository.query(callQueryStringEmp);
      return results[0];
    } catch (error) {
      console.error('An error occurred:', error);
    }
  }

  // const results = await this.employeeRepository.query(queryString);

  // const results = await this.employeeRepository.query(
  //   `SET @p0='${name}'; SET @p1='${tel}'; SET @p2='${salary}'; SET @p3='${dateIn}'; SET @p4='${status}'; SET @p5='${branch}'; SET @p6='Result'; SET @p7='Message'; CALL insertEmployee(@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7); SELECT @p6 AS pResult, @p7 AS pMessage `,
  // );

  // `CALL getEmployee('${a}','${b}')`,
  // SET @p0='Mild'; SET @p1='0823456711'; SET @p2='10000.00'; SET @p3='2024-04-02'; SET @p4='1'; SET @p5='1'; SET @p6='Result'; SET @p7='Message'; CALL `insertEmployee`(@p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7); SELECT @p6 AS `pResult`, @p7 AS `pMessage`;

  findOne(id: number) {
    return this.employeeRepository.findOne({
      where: { id },
      relations: { status: true, branch: true },
    });
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const employee = await this.employeeRepository.findOneOrFail({
      where: { id },
    });
    employee.name = updateEmployeeDto.name;
    employee.tel = updateEmployeeDto.tel;
    employee.salary = parseInt(updateEmployeeDto.salary);
    employee.dateIn = updateEmployeeDto.dateIn;
    employee.status = JSON.parse(updateEmployeeDto.status);
    if (updateEmployeeDto.image && updateEmployeeDto.image !== '') {
      employee.image = updateEmployeeDto.image;
    }
    this.employeeRepository.save(employee);
    const result = await this.employeeRepository.findOne({
      where: { id },
      relations: { status: true },
    });
    return result;
  }
  async remove(id: number) {
    const deleteEmployee = await this.employeeRepository.findOneOrFail({
      where: { id },
    });
    await this.employeeRepository.remove(deleteEmployee);

    return deleteEmployee;
  }
}
