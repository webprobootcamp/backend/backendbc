/* eslint-disable prettier/prettier */
// import {
//   Controller,
//   Get,
//   Post,
//   Body,
//   Param,
//   Delete,
//   UseInterceptors,
//   UploadedFile,
//   UseGuards,
// } from '@nestjs/common';
// import { EmployeesService } from './employees.service';
// import { CreateEmployeeDto } from './dto/create-employee.dto';
// import { FileInterceptor } from '@nestjs/platform-express';
// import { diskStorage } from 'multer';
// import { uuid } from 'uuidv4';
// import { extname } from 'path';
// import { UpdateEmployeeDto } from './dto/update-employee.dto';
// import { AuthGuard } from 'src/auth/auth.guard';

// @UseGuards(AuthGuard)
// @Controller('employees')
// export class EmployeesController {
//   constructor(private readonly employeesService: EmployeesService) {}

//   @Post()
//   @UseInterceptors(
//     FileInterceptor('file', {
//       storage: diskStorage({
//         destination: './public/images/Employees',
//         filename: (req, file, cb) => {
//           const name = uuid();
//           return cb(null, name + extname(file.originalname));
//         },
//       }),
//     }),
//   )
//   create(
//     @Body() createEmployeeDto: CreateEmployeeDto,
//     @UploadedFile() file: Express.Multer.File,
//   ) {
//     if (file) {
//       createEmployeeDto.image = file.filename;
//     }
//     return this.employeesService.create(createEmployeeDto);
//   }

//   @Get()
//   findAll() {
//     return this.employeesService.findAll();
//   }

//   @Get(':id')
//   findOne(@Param('id') id: string) {
//     return this.employeesService.findOne(+id);
//   }

//   @Post(':id')
//   @UseInterceptors(
//     FileInterceptor('file', {
//       storage: diskStorage({
//         destination: './public/images/employees',
//         filename: (req, file, cb) => {
//           const name = uuid();
//           return cb(null, name + extname(file.originalname));
//         },
//       }),
//     }),
//   )
//   update(
//     @Param('id') id: string,
//     @Body() updateEmployeeDto: UpdateEmployeeDto,
//     @UploadedFile() file: Express.Multer.File,
//   ) {
//     console.log(file);

//     if (file) {
//       updateEmployeeDto.image = file.filename;
//     }
//     return this.employeesService.update(+id, updateEmployeeDto);
//   }

//   @Delete(':id')
//   remove(@Param('id') id: string) {
//     return this.employeesService.remove(+id);
//   }

//   @Post('upload')
//   @UseInterceptors(
//     FileInterceptor('file', {
//       storage: diskStorage({
//         destination: './public/img/employees',
//         filename: (req, file, cb) => {
//           const name = uuid();
//           return cb(null, name + extname(file.originalname));
//         },
//       }),
//     }),
//   )
//   uploadFile(
//     @Body() employee: { name: string; salary: number },
//     @UploadedFile() file: Express.Multer.File,
//   ) {
//     console.log(employee);
//     console.log(file.filename);
//     console.log(file.path);
//   }
// }
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { uuid } from 'uuidv4';
import { extname } from 'path';

@Controller('employees')
export class EmployeesController {
  constructor(private readonly EmployeeService: EmployeesService) {}
  @Post('storeEmploy')
  findAllspp(@Body() createEmployeeDto: CreateEmployeeDto) {
    return this.EmployeeService.findAllspp(
      createEmployeeDto.name,
      createEmployeeDto.tel,
      createEmployeeDto.salary,
      createEmployeeDto.dateIn,
      createEmployeeDto.status,
      createEmployeeDto.branch,
    );
  }
  @Post('storeCountEmploy')
  findAllsppp(@Body() createEmployeeDto: CreateEmployeeDto) {
    return this.EmployeeService.findAllsppp(
      createEmployeeDto.name,
      createEmployeeDto.tel,
      createEmployeeDto.salary,
      createEmployeeDto.dateIn,
      createEmployeeDto.status,
      createEmployeeDto.branch,
    );
  }
  // Create
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/employees',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createEmployeeDto: CreateEmployeeDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      createEmployeeDto.image = file.filename;
    }
    return this.EmployeeService.create(createEmployeeDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.EmployeeService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.EmployeeService.findOne(+id);
  }
  // Partial Update
  @Post(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/employees',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updateEmployeeDto: UpdateEmployeeDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updateEmployeeDto.image = file.filename;
    }
    return this.EmployeeService.update(+id, updateEmployeeDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.EmployeeService.remove(+id);
  }
}
