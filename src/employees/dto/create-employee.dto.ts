export class CreateEmployeeDto {
  name: string;
  tel: string;
  salary: string;
  dateIn: string;
  image: string;
  status: string;
  result?: number;
  branch?: string;
}
