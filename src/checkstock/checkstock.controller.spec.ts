import { Test, TestingModule } from '@nestjs/testing';
import { CheckstocksController } from './checkstock.controller';
import { CheckstocksService } from './checkstock.service';

describe('CheckstocksController', () => {
  let controller: CheckstocksController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckstocksController],
      providers: [CheckstocksService],
    }).compile();

    controller = module.get<CheckstocksController>(CheckstocksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
