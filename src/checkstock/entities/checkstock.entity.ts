import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { CheckstockItem } from './checkstockitem.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class Checkstock {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => CheckstockItem,
    (checkstockItems) => checkstockItems.checkstock,
  )
  checkstockItems: CheckstockItem[];

  @ManyToOne(() => User, (user) => user.Checkstocks)
  user: User;
}
