import { Module } from '@nestjs/common';
import { CheckstocksService } from './checkstock.service';
import { CheckstocksController } from './checkstock.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkstock } from './entities/checkstock.entity';
import { CheckstockItem } from './entities/checkstockitem.entity';
import { Stock } from 'src/stocks/entities/stock.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Checkstock, CheckstockItem, Stock])],
  controllers: [CheckstocksController],
  providers: [CheckstocksService],
})
export class CheckstocksModule {}
