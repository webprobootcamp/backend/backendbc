import { Test, TestingModule } from '@nestjs/testing';
import { CheckstocksService } from './checkstock.service';

describe('CheckstocksService', () => {
  let service: CheckstocksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckstocksService],
    }).compile();

    service = module.get<CheckstocksService>(CheckstocksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
